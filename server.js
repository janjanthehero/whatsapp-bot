// You can find your Twilio Auth Token here: https://www.twilio.com/console
// Set at runtime as follows:
// $ TWILIO_AUTH_TOKEN=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx node index.js
//
// This will not work unless you set the TWILIO_AUTH_TOKEN environment
// variable.

require('dotenv').config(); // Loads all our environment variables from the .env file

const twilio = require('twilio');
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const MessagingResponse = require('twilio').twiml.MessagingResponse;

mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.connect(process.env.DATABASE_URL);
const db = mongoose.connection;
db.on('error', (error) => console.error(error));
db.once('open', () => console.log('Connected to database'));

app.use(express.json()); // Allows our server accept json as a body inside a post, get or whatever element

const punchinCardRouter = require('./routes/punchcards');
app.use('/punchcards', punchinCardRouter);

// Twilio auth token environment variable
process.env.TWILIO_AUTH_TOKEN;

const shouldValidate = process.env.NODE_ENV !== 'test';

app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', (req, res) => res.send("Hello From Server"));

app.post(
'/',
twilio.webhook({ validate: shouldValidate }),
(req, res) => {

    // Twilio Messaging URL - receives incoming messages from Twilio
    const response = new MessagingResponse();

    switch(req.body.Body){
        case "start":
        case "Start":
            response.message('You have punched IN');
            break;
        case "finish":
        case "Finish":
            response.message('You have punched OUT');
            break;
        case "help":
        case "Help":
            response.message('Type "start" to punch in, or "finish" to punch out');
            break;
        default:
            response.message('Invalid Command. Type "Help" for available commands');
    }

    res.set('Content-Type', 'text/xml');
    res.send(response.toString());

    // Console log the received message
    console.log(req.body.Body);
}
);

app.listen(3000, function (){
    console.log('Sever running and listening on port 3000...')
});