const mongoose = require('mongoose');

const punchCardSchema = new mongoose.Schema({
    punchinType: {
        type: String,
        required: true,
    },
    punchinTime: {
        type: Date,
        required: true,
        default: Date.now
    }
})

module.exports = mongoose.model('Punchcard', punchCardSchema);