const express = require('express');
const router = express.Router();
const Punchcard = require('../models/punchcard');

// Get Punchcards
router.get('/', async(req, res) => {
    try {
        const punchcards = await Punchcard.find();
        res.json(punchcards);
    } catch (err) {
        res.status(500).json({message: err.message});
    }
})

// Punch in/our
router.post('/', async(req, res) => {
    const punchcard = new Punchcard({
        punchinType: req.body.punchinType,

    });

    try {
        const newPunchcard = await punchcard.save();
        res.status(201).json(newPunchcard);
    } catch(err) {
        res.status(400).json({ message: err.message });
    }
})

module.exports = router;